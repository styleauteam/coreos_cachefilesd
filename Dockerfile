FROM alpine:edge
MAINTAINER Stephan Wienczny <stephan.wienczny@ybm-deutschland.de>

RUN echo "@testing http://nl.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories
RUN apk add --no-cache cachefilesd@testing
VOLUME ["/fscache"]
CMD ["/usr/bin/cachefilesd", "-s", "-n", "-f", "/etc/cachefilesd.conf"]
